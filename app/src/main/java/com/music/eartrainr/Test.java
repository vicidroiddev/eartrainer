package com.music.eartrainr;

import java.util.ArrayList;
import java.util.List;


public class Test {


  public static List<String> getFriendList() {

    List friends = new ArrayList(10);

    friends.add("Raphael");
    friends.add("Jacinta");
    friends.add("Nicole");
    friends.add("Adrian");
    friends.add("Michael");
    friends.add("Nick");
    friends.add("Victor");
    friends.add("Jordan");
    friends.add("Nathan");

    return friends;

  }
}
